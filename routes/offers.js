var express = require('express');
var router = express.Router();
var db = require('../database/db');
var request = require("request");

var tcURLS = {
  8: "https://transportcompany-g.mxapps.io",
  9: "https://newtransportcompan.mxapps.io",
  10: "https://transcom20.mxapps.io",
  11: "https://tc11.mxapps.io"
};

var SOAPNotification = {
  start: '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://www.example.com/"><soap:Body><tns:CDB_Update>',
  orderId: {start: '<OrderID>', end: '</OrderID>'},
  orderSourceNumber: {start: '<OrderSourceNumber>', end: '</OrderSourceNumber>'},
  secondOrderId: {start: '<SecondOrderId>', end: '</SecondOrderId>'},
  secondOrderSourceNumber: {start: '<SecondOrderSourceNumber>', end: '</SecondOrderSourceNumber>'},
  match: {start: '<Match>', end: '</Match>'},
  end: '</tns:CDB_Update></soap:Body></soap:Envelope>'
};

var notifications = {};

router.get('/all', function(req, res) {
  db.getAllOrders(function(err, results) {
    if (err) return internalServerError(res, err.message);
    return res.json(results);
  })
});
router.get('/', function(req, res) {
  if (req.query.orderId && req.query.orderSourceNumber) {
    var orderIdentifier = [
      req.query.orderId,
      req.query.orderSourceNumber
    ];
    db.getOrderById(orderIdentifier, function(err, results) {
      if (err) return internalServerError(res, err.message);
      if (!results[0]) return notFound(res, 'No order found for orderId: ' + req.query.orderId + ' and orderSourceNumber: ' + req.query.orderSourceNumber);
      return res.json(results[0]);
    });
  } else {
    db.getValidOrders(function (err, results) {
      if (err) return internalServerError(res, err.message);
      return res.json(results);
    });
  }
});
router.post('/new', function(req, res) {
  if (!req.body)                    return badRequest(res, "No request body");
  if (!req.body.orderId)            return badRequest(res, "No orderId in request");
  if (!req.body.orderSourceNumber)  return badRequest(res, "No orderSourceNumber in request");
  if (!req.body.price)              return badRequest(res, "No price in request");
  if (!req.body.volume)             return badRequest(res, "No volume in request");
  if (!req.body.startPoint)         return badRequest(res, "No startPoint in request");
  if (!req.body.endPoint)           return badRequest(res, "No endPoint in request");
  if (!req.body.truckCompany)       return badRequest(res, "No truckCompany in request");

  var newOrder = {
    orderId: req.body.orderId,
    orderSourceNumber: req.body.orderSourceNumber,
    price: req.body.price,
    volume: req.body.volume,
    startPoint: req.body.startPoint,
    endPoint: req.body.endPoint,
    truckCompany: req.body.truckCompany,
    status: 0,
    createdAt: new Date(),
    expiresAt: addMinutes(new Date(), 10)
  };

  db.insertNewOrder(newOrder, function(err) {
    if (err) {
      console.log("Order failed to insert: " + newOrder);
      return internalServerError(res, err.message);
    }
    scheduleTruckCompanyNotificationForOrder(newOrder);
    return res.json({'success': true});
  });
});
router.post('/accept', function(req, res) {
  if (!req.body)                          return badRequest(res, "No request body");
  if (!req.body.orderId)                  return badRequest(res, "No orderId in request");
  if (!req.body.orderSourceNumber)        return badRequest(res, "No orderSourceNumber in request");
  if (!req.body.secondOrderId)            return badRequest(res, "No secondOrderId in request");
  if (!req.body.secondOrderSourceNumber)  return badRequest(res, "No secondOrderSourceNumber in request");
  if (!req.body.secondTruckCompany)       return badRequest(res, "No secondTruckCompany in request");
  if (!req.body.secondVolume)             return badRequest(res, "No secondVolume in request");
  if (!req.body.secondStartPoint)         return badRequest(res, "No secondStartPoint in request");
  if (!req.body.secondEndPoint)           return badRequest(res, "No secondEndPoint in request");

  var acceptOrderInfo = [
    req.body.secondOrderId,
    req.body.secondOrderSourceNumber,
    req.body.secondTruckCompany,
    req.body.secondVolume,
    req.body.secondStartPoint,
    req.body.secondEndPoint,
    req.body.orderId,
    req.body.orderSourceNumber
  ];

  db.acceptOrder(acceptOrderInfo, function(err, result) {
    if (err) {
      console.log("Order failed to accept: " + acceptOrderInfo);
      return internalServerError(res, err.message);
    }
    if (result.affectedRows !== 1) return gone(res, "The time for this load has expired or it is already taken");
    db.getOrderById([req.body.orderId, req.body.orderSourceNumber], function(err, result) {
      if (err) return console.log(err.message);
      var order = result[0];
      if (!order) return console.log("Order not found");
      var notifyId = order.orderId + order.orderSourceNumber;
      notifications[notifyId] = false;
      notifyTruckCompany(order, true);
    });
    return res.json({'success': true});
  });
});

function scheduleTruckCompanyNotificationForOrder(orderInfo, timeMilliseconds) {
  // if (!timeMilliseconds) timeMilliseconds = 5000; // 5 seconds
  if (!timeMilliseconds) timeMilliseconds = 10 * 60 * 1000; // ten minutes
  var notifyId = orderInfo.orderId + orderInfo.orderSourceNumber;
  notifications[notifyId] = true;
  setTimeout(function() {
    if (notifications[notifyId]) {
      notifyTruckCompany(orderInfo, false);
    }
  }, timeMilliseconds);
}

function notifyTruckCompany(orderInfo, match) {
  var host = tcURLS[orderInfo.truckCompany];
  var path = "/ws/CDB_Update";
  var secondOrderId = orderInfo.secondOrderId ? orderInfo.secondOrderId : 0;
  var secondOrderSourceNumber = orderInfo.secondOrderSourceNumber ? orderInfo.secondOrderSourceNumber : 0;
  var body = SOAPNotification.start +
    SOAPNotification.orderId.start + orderInfo.orderId + SOAPNotification.orderId.end +
    SOAPNotification.orderSourceNumber.start + orderInfo.orderSourceNumber + SOAPNotification.orderSourceNumber.end +
    SOAPNotification.secondOrderId.start + secondOrderId + SOAPNotification.secondOrderId.end +
    SOAPNotification.secondOrderSourceNumber.start + secondOrderSourceNumber + SOAPNotification.secondOrderSourceNumber.end +
    SOAPNotification.match.start + match + SOAPNotification.match.end +
    SOAPNotification.end;
  var options = {
    url: host + path,
    method: "POST",
    body: body,
    headers: {"Content-Type": "application/xml"}
  };
  console.log(orderInfo, match, body);
  request(options, function (error, response, body) {
    console.log(error, body);
  });
}

// REQUEST HELPERS
function badRequest(res, message) {
  return res.status(400).send({error: message});
}
function notFound(res, message) {
  return res.status(404).send({error: message});
}
function internalServerError(res, message) {
  return res.status(500).send({error: message});
}
function gone(res, message) {
  return res.status(410).send({error: message});
}

// DATE HELPERS
function addMinutes(date, minutes) {
  return new Date(date.getTime() + minutes*60000);
}

module.exports = router;