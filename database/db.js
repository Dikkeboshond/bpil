var mysql      = require('mysql');
require('dotenv').config();
var connection = null;
var dbConfig = {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  acquireTimeout: process.env.DB_TIMEOUT
};

function select(query, callback) {
  var connection = getConnection();
  connection.query(query, function(err, rows) {
    if (err) return callback(err);

    return callback(null, rows);
  });
  disconnect();
}

function selectWhere(query, where, callback) {
  var connection = getConnection();
  connection.query(query, where, function(err, rows) {
    if (err) return callback(err);
    return callback(null, rows);
  });
  disconnect();
}

function insert(query, values, callback) {
  var connection = getConnection();
  connection.query(query, values, function(err, rows) {
    if (err) return callback(err);

    return callback(null, rows);
  });
  disconnect();
}

function update(query, values, callback) {
  var connection = getConnection();
  connection.query(query, values, function(err, rows) {
    if (err) return callback(err);

    return callback(null, rows);
  });
}

exports.getOrderById = function(identifiers, callback) {
  selectWhere('SELECT * FROM bpil.`order` WHERE orderId = ? AND orderSourceNumber = ?', identifiers, function(err, rows) {
    if (err) return callback(err);
    return callback(null, rows);
  });
};

exports.getAllOrders = function(callback) {
  select('SELECT * FROM bpil.`order`;', function(err, rows) {
    if (err) return callback(err);
    return callback(null, rows);
  });
};

exports.getValidOrders = function(callback) {
  select('SELECT orderId, orderSourceNumber, price, volume, startPoint, endPoint, truckCompany, expiresAt FROM bpil.`order` WHERE status = 0 AND expiresAt > now();', function(err, rows) {
    if (err) return callback(err);
    return callback(null, rows);
  });
};

exports.insertNewOrder = function(newOrder, callback) {
  insert('INSERT INTO `bpil`.`order` SET ?', newOrder, function(err) {
    if (err) return callback(err);
    return callback(null);
  });
};

exports.acceptOrder = function(orderData, callback) {
  update('UPDATE `order` SET status = 1, secondOrderId = ?, secondOrderSourceNumber = ?, secondTruckCompany = ?, secondVolume = ?, secondStartPoint = ?, secondEndPoint = ? WHERE orderId = ? AND orderSourceNumber = ? AND status = 0 AND expiresAt > now()', orderData, function(err, rows, fields) {
    if (err) return callback(err);
    return callback(null, rows, fields);
  })
};

function getConnection() {
  if (connection == null) {
    connection = mysql.createConnection(dbConfig);
  }
  return connection;
}

function disconnect() {
  connection.end(function(err) {
    if (err) console.log("Disconnect: connection.end - " + err);
    connection = null;
  });
}
